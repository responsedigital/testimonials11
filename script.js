class Testimonials11 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initTestimonials11()
    }

    initTestimonials11 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Testimonials11")
    }

}

window.customElements.define('fir-testimonials-11', Testimonials11, { extends: 'div' })
