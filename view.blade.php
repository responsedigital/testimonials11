<!-- Start Testimonials11 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Blockquote with overlapping image. -->
@endif
<div class="testimonials-11 {{ $classes }}"  is="fir-testimonials-11">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="testimonials-11__wrap">
    <div class="testimonials-11__content">
      <svg width="46px" height="44px" viewBox="0 0 46 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Testimonials" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Testimonials_11_1440" transform="translate(-141.000000, -124.000000)" fill-rule="nonzero">
              <rect id="Rectangle" fill="#FFFFFF" x="0" y="0" width="1440" height="592"></rect>
              <path d="M186.256,160.088 L188.148,124.14 L169.228,124.14 L171.292,160.088 L186.256,160.088 Z M158.564,160.088 L160.628,124.14 L141.708,124.14 L143.6,160.088 L158.564,160.088 Z" id="&quot;" fill="#0076FF"></path>
          </g>
      </g>
      </svg>
      <blockquote class="testimonials-11__quote">
        <h4>{!! $quote ?: $faker->paragraph($nbSentences = 4, $variableNbSentences = true) !!}</h4>
      </blockquote>
      <h6 class="testimonials-11__person">{{ $person ?: $faker->name }}</h6>
      <p class="testimonials-11__company">{{ $company ?: $faker->text($maxNbChars = 20) }}</p>
    </div>
    <img class="testimonials-11__image" src="{{ $image['url'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg'}}" alt="">
  </div>
</div>
<!-- End Testimonials11 -->
