<?php

namespace Fir\Pinecones\Testimonials11;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Testimonials11',
            'label' => 'Pinecone: Testimonials11',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Blockquote with overlapping image."
                ],
                [
                    'label' => 'Quote',
                    'name' => 'quoteTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Quote',
                    'name' => 'quote',
                    'type' => 'textarea'
                ],
                [
                    'label' => 'Person',
                    'name' => 'person',
                    'type' => 'text'
                ],
                [
                    'label' => 'Company',
                    'name' => 'company',
                    'type' => 'text'
                ],
                [
                    'label' => 'Image',
                    'name' => 'imageTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'flipHorizontal',
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
